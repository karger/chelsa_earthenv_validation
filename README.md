CHELSA_EarthEnv_validation
-----------
This module contains code to create the figures in
Karger, D.N., Wilson, A.M., Mahony, C., Zimmermann, N.E., Jetz, W. (2021) Global daily 1km land surface precipitation based on cloud cover-informed downscaling. Scientific Data. doi.org/10.1038/s41597-021-01084-6

It is part of the CHELSA Project: (CHELSA, <https://www.chelsa-climate.org/>).




COPYRIGHT
---------
(C) 2021 Dirk Nikolaus Karger



LICENSE
-------
CHELSA_EarthEnv_validation is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CHELSA_EarthEnv_validation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with CHELSA_EarthEnv_validation. If not, see <http://www.gnu.org/licenses/>.



REQUIREMENTS
------------
CHELSA_EarthEnv_validation is written in R


HOW TO USE
----------
Each script needs to be run solitarily.


CITATION:
------------
If you need a citation for the output, please refer to the arcticle describing the high
resolution data:

DN Karger, AM Wilson, C Mahony, NE Zimmermann, W Jetz 'Global daily 1km land surface precipitation based on cloud cover-informed downscaling', arXiv preprint arXiv:2012.10108


CONTACT
-------
<dirk.karger@wsl.ch>



AUTHOR
------
Dirk Nikolaus Karger
Swiss Federal Research Institute WSL
Zürcherstrasse 111
8903 Birmensdorf 
Switzerland
