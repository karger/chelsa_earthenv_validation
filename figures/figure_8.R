#!/usr/bin/env R

#This file is part of CHELSA_EarthEnv_validation.
#
#CHELSA_EarthEnv_validation is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#CHELSA_EarthEnv_validation is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with CHELSA_EarthEnv_validation.  If not, see <https://www.gnu.org/licenses/>.


library(RPostgreSQL)
library(raster)
library(maptools)
library(rgdal)
library(reshape)
library(ggplot2)


### PostgreSQL connection
pg = dbDriver("PostgreSQL")
con = dbConnect(pg, user="****", password="****",host="postgres.wsl.ch", port=5432, dbname="ghcn")


SQL_Statement <-paste0("SELECT count(*) AS n, avg(CAST(d.value AS numeric))/10 AS value, s.latitude, s.longitude , s.id
                       FROM ghcnd.daydat d, ghcnd.stations s 
                       WHERE d.id = s.id 
                       AND d.element = \'PRCP\'
                       AND d.year > 2002
                       AND d.year < 2017
                       AND d.value BETWEEN 0 AND 30000
                       AND d.value IS NOT NULL
                       AND d.qflag = ''
                       GROUP BY s.id, d.element, d.year, d.month
                       HAVING count(*) > 25
                       ORDER BY s.latitude, s.longitude")

cat(paste0("### get prec values from GHCN DB...\n"))
ghcn <- dbGetQuery(con,SQL_Statement)

hawaii_agg1 <- aggregate(.~longitude+latitude,ghcn[,1:4],mean)
coordinates(hawaii_agg1) <- ~longitude+latitude

templatename <- "/storage/karger/chelsa_V2/OUTPUT_DAILY/tz/CHELSA_tz_25_02_2003_V.2.1.tif"
template <- raster(templatename)

pdf(paste0("/home/karger/Documents/bio/Bio_Backup/Manuskripts/CHELSA_V2_Land/figure_8.pdf"),
    height=8,
    width = 12)


ex_hawaii <- extent(-156.15,
                    -154.5,
                    18.8,
                    20.5)


par(mfrow=c(2,3),
    mar=c(5,4,2,1), 
    oma=c(2,4,1,1))


cords<-c(-156.15,
         -154.5,
         18.75,
         18.7)


cx1 = 1.5
lg1 = 5
lb1 = 3
tg1 = 1.5
cxm = 1.5
pchx = 1

las = 2
lasx = 3
cexmain = 1.5
lasmain = 1
cexlab = 1.2
cxl = 3

ghcn_hawaii<-crop(hawaii_agg1,
                  ex_hawaii)

shapefile(ghcn_hawaii, 
          file="/mnt/lud11/karger/chelsa_modcf_validation/hawaii/annual_mean_clim_ghcn.shp", 
          overwrite=T)

coast<-shapefile("/mnt/lud11/karger/chelsa_modcf_validation/hawaii/coastll010g.shp")

coast<-crop(coast,ex_hawaii)

rbPal <- colorRampPalette(c('blue','grey80','red'))

bbreaks<-c(-99999,-1.5,-1,-0.5,-0.1,0.1,0.5,1,1.5,99999)

cbreaks=c(0,1,2,3,4,5,10,15,99999900)

ghcn_hawaii$Col <- rev(sequential_hcl((length(cbreaks)-1), 
                       "Purple-Yellow", 
                       p1 = 1.3, 
                       c2 = 20))[as.numeric(cut(ghcn_hawaii$value, breaks=cbreaks))]

chelsa_land <- raster("/mnt/lud11/karger/chelsa_modcf_validation/hawaii/annual_mean_clim_chv2land.tif")

chelsa <- raster("/mnt/lud11/karger/chelsa_modcf_validation/hawaii/annual_mean_clim_chv2.tif")

chelsa_land <- crop(chelsa_land, ex_hawaii)

chelsa <- crop(chelsa, ex_hawaii)

era5 <- aggregate(chelsa, fact=25, mean)


plot(era5/365.15,
     col=rev(sequential_hcl((length(cbreaks)-1), 
     "Purple-Yellow", 
     p1 = 1.3, 
     c2 = 20)), 
     breaks=cbreaks, 
     axes = F,
     ylab = "",
     xlab = "",
     ex.lab=cexlab, 
     box=F,
     legend=F)


title('without downscaling', 
      line=lasmain, 
      cex.main=cexmain)


points(ghcn_hawaii, 
       col=ghcn_hawaii$Col, 
       pch=19, 
       cex=pchx)


points(ghcn_hawaii, 
       pch=21, 
       cex=pchx)

plot(coast, 
     add=T, 
     col='grey25')


plot(chelsa/365.15,
     col=rev(sequential_hcl((length(cbreaks)-1), 
     "Purple-Yellow", 
     p1 = 1.3, 
     c2 = 20)), 
     breaks=cbreaks, 
     axes = F,
     ylab = "",
     xlab = "",
     cex.lab=cexlab, 
     box=F, 
     legend=F)

title('without cloud refinement', 
      line=lasmain, 
      cex.main=cexmain)

points(ghcn_hawaii, 
       col=ghcn_hawaii$Col, 
       pch=19, 
       cex=pchx)

points(ghcn_hawaii, 
       pch=21, 
       cex=pchx)

plot(coast, 
    add=T, 
    col='grey25')

par(xpd=T)


cols<-rev(sequential_hcl((length(cbreaks)-1), 
          "Purple-Yellow", 
          p1 = 1.3, 
          c2 = 20))

try(cscl(cols,cords,zrng=c(0,7),at = c(),tria = "u", horiz = T, lablag=1, cx=cx1))

try(cscl(cols[1:(length(cols))-1],cords,zrng=c(0,7),at = seq(0,7,by=1),tria = "n", horiz = T,
         labs=c("0","1","2","3","4","5","10",">15"),  cx=cx1,
         title=expression(paste("", kg," ",m^{-2},'',day^{-1},'')),titlag=lg1,tickle=tg1,lablag=lb1))

par(xpd=F)


plot(chelsa_land/365.15,
     col=rev(sequential_hcl((length(cbreaks)-1), 
     "Purple-Yellow", 
     p1 = 1.3, 
     c2 = 20)), 
     breaks=cbreaks,  
     axes = F,
     ylab = "",
     xlab = "",
     cex.lab=cexlab, 
     box=F, 
     legend=F)


title('with cloud refinement', 
      line=lasmain, 
      cex.main=cexmain)


points(ghcn_hawaii, 
       col=ghcn_hawaii$Col, 
       pch=19, 
       cex=pchx)


points(ghcn_hawaii, 
       pch=21, 
       cex=pchx)


plot(coast, 
     add=T, 
     col='grey25')


###################### scatterplots
ghcn_hawaii$chelsa <- extract(chelsa,
                              ghcn_hawaii)


ghcn_hawaii$chelsa_land <- extract(chelsa_land,
                                   ghcn_hawaii)


ghcn_hawaii$era5<-extract(era5,
                          ghcn_hawaii)


df2 <- as.data.frame(ghcn_hawaii)

df2$chelsa <- df2$chelsa/365.15

df2$chelsa_land <- df2$chelsa_land/365.15
df2$era5 <- df2$era5/365.15

par(xpd=T)
plot(df2$value,
     df2$era5,  
     ylim=c(0,20), 
     xlim=c(0,20), 
     pch=19, 
     yaxs="i",
     xaxs="i",
     xlab="", las=1,
     ylab="",
     cex.axis=cexlab)

title(ylab=expression(paste("precicted ", kg," ",m^{-2},'',day^{-1},'')), line=las, cex.lab=1.5)

par(xpd=F)

abline(0,1,col="red")

r_1 <- cor(df2$value, df2$era5)

text(paste0('r = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=5, 
     pos=2, 
     cex=2, 
     adj=0)

r_1<-mae(df2$value, df2$era5)

text(paste0('mae = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=3, 
     pos=2, 
     cex=2, 
     adj=0)

r_1 <- rmse(df2$value, df2$era5)

text(paste0('rmse = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=1, 
     pos=2, 
     cex=2, 
     adj=0)

plot(df2$value,
     df2$chelsa, 
     ylim=c(0,20), 
     xlim=c(0,20), 
     pch=19, 
     yaxs="i",
     xaxs="i",
     xlab="",
     las=1,
     ylab="",
     cex.axis=cexlab)

par(xpd=T)

title(xlab=expression(paste("observed ", kg," ",m^{-2},'',day^{-1},'')), 
      line=lasx, 
      cex.lab=1.5)

par(xpd=F)

abline(0,1,col="red")

r_1<-cor(df2$value, df2$chelsa)

text(paste0('r = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=5, 
     pos=2, 
     cex=2, 
     adj=0)

r_1<-mae(df2$value, df2$chelsa)

text(paste0('mae = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=3, 
     pos=2, 
     cex=2, 
     adj=0)

r_1<-rmse(df2$value, df2$chelsa)

text(paste0('rmse = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=1, 
     pos=2, 
     cex=2, 
     adj=0)


plot(df2$value,
     df2$chelsa_land, 
     ylim=c(0,20), 
     xlim=c(0,20), 
     pch=19,
     xlab="",
     ylab="", 
     yaxs="i",
     xaxs="i",
     cex.axis=cexlab, 
     las=1)

abline(0,1,col="red")

r_1<-cor(df2$value, df2$chelsa_land)

text(paste0('r = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=5, 
     pos=2, 
     cex=2, 
     adj=0)

r_1<-mae(df2$value, df2$chelsa_land)

text(paste0('mae = ',format(round(r_1,2),nsmall=2)), 
    x=19.5,
    y=3, 
    pos=2, 
    cex=2, 
    adj=0)

r_1<-rmse(df2$value, df2$chelsa_land)

text(paste0('rmse = ',format(round(r_1,2),nsmall=2)), 
     x=19.5,
     y=1, 
     pos=2, 
     cex=2, 
     adj=0)

dev.off()




