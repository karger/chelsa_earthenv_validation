//This file is part of CHELSA_EarthEnv_validation.
//
//CHELSA_EarthEnv_validation is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//CHELSA_EarthEnv_validation is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with CHELSA_EarthEnv_validation.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////
// John Wilshire 2020-10-18
// Calculting a Chelsa v2.1 Annual Climatology
// No pixel qc filtering 
////////////////////////////
  
  var palettes = require('users/gena/packages:palettes');
  var viridis = palettes.matplotlib.viridis[7];
  ////////////////////////////
    // Constants
  ////////////////////////////
    var COLLECTION = ee.ImageCollection('projects/earthenv/chelsa/daily_precip_land_v21');
    var xmin = -163; 
    var xmax = -154;
    var ymin = 18;
    var ymax = 23;
    
    ////////////////////////////
      // Functions
    ////////////////////////////
      var geometryFromBounds = function (xmin, xmax, ymin, ymax) {
        return ee.Geometry({
          type: "Polygon",
          coordinates: [
            [
              [
                xmin,
                ymax
                ],
              [
                xmin,
                ymin
                ],
              [
                xmax,
                ymin
                ],
              [
                xmax,
                ymax
                ],
              [
                xmin,
                ymax
                ]
              ]
            ]
        }, undefined, false)
      };
      
      
      var reduceForYear = function (
        collection,
        region,
        year,
        scaleFactor,
        reducer){
        if (!scaleFactor) scaleFactor = 100;
        if (!reducer) reducer = ee.Reducer.mean();
        return collection.filterBounds(region)
        //  filter date is end date exclusive
        .filterDate(ee.Date.fromYMD(year, 1, 1), ee.Date.fromYMD(ee.Number(year).add(1), 1, 1))
        .map(function(x) {
          return x.clip(region).multiply(scaleFactor);
        })
        .reduce(reducer);
      };
      
      ////////////////////////////
        var aoi = geometryFromBounds(xmin, xmax, ymin, ymax);
        Map.centerObject(aoi, 6)
        var years = ee.List.sequence(2003, 2016, 1);
        
        var perYear = years.map(function(year) {
          return reduceForYear(COLLECTION, aoi, year, 0.01, ee.Reducer.sum());
        })
        perYear = ee.ImageCollection.fromImages(perYear)
        
        // Reduce the list of yearly sums into an annual mean.
        
        var annualClimatology = ee.Image(perYear.reduce(ee.Reducer.mean()))
        
        Map.addLayer(annualClimatology, {min: 0, max: 5000, palette: viridis}, 'Annual climatology (kg*m^-2*day^-1)');
        
        
        var geometry = ee.Geometry.Rectangle([xmin, ymin, xmax, ymax]);
        
        Export.image.toDrive({
          image: annualClimatology,
          description: 'annual_mean_clim',
          fileFormat: 'GeoTIFF',
          folder: 'cloud',
          scale: 1000,
          region: geometry,
          formatOptions: {  
            cloudOptimized: true  
          }  
        });
        
        ////////////////////////////
          // QC 
        ////////////////////////////
          print(aoi.area(1).divide(1e6), 'km2')
        // Map.addLayer(aoi)
        
        // For each year calculate the min and max image date, the total number of images, and the number of distinct dates. 
        
        var yearQC = years.map(function(year) {
          var yearlyCollection = COLLECTION.filterDate(
            ee.Date.fromYMD(year, 1, 1),
            ee.Date.fromYMD(ee.Number(year).add(1), 1, 1))
          return ee.Dictionary({
            imageCount: yearlyCollection.size(),
            distinctDates: yearlyCollection.aggregate_count_distinct('system:time_start'),
            minDate: ee.Date(yearlyCollection.aggregate_min('system:time_start')).format(),
            maxDate: ee.Date(yearlyCollection.aggregate_max('system:time_start')).format(),
            year: year
          })
        })
        print('yearQC', yearQC)
        
        
        perYear.getVideoThumbURL({
          min: 0, max: 10000, dimensions: 1000,
          framesPerSecond: 1,
          palette: viridis,
          region: aoi.toGeoJSON()
        }, function(url) {
          print('Animation url:', url)
        })
        
        
        
